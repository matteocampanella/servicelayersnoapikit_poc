package servicelayersnoapikit_poc;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/services")
public class ApplicationLayerServices {
	@POST
	@Path("/serviceAppOne/{parm}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ApplicationOneResponse serviceOne(@PathParam("parm") String parm, ApplicationOneRequest request) {
		
		System.out.println(parm);
		System.out.println(request.getProperty1());
		System.out.println(request.getProperty2());
		
		ServiceOneRequest soreq = new ServiceOneRequest();
		soreq.setProperty1(request.getProperty1());
		soreq.setProperty2(request.getProperty2());
		Client client = ClientBuilder.newClient();
		Response response = client.target("http://localhost:8081")
				.path("/servicelayer/v1.0/services/serviceOne/".concat(parm))
				.request(MediaType.APPLICATION_JSON)
				.post(Entity.json(request));
		ServiceOneResponse soresp = response.readEntity(ServiceOneResponse.class);
		
		ApplicationOneResponse sr = new ApplicationOneResponse();
		sr.setProperty1(soresp.getProperty1().toUpperCase());
		sr.setProperty2(soresp.getProperty2().toUpperCase());
		
		return sr;
	}
}
