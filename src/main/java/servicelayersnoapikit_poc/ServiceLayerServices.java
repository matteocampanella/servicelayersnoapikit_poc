package servicelayersnoapikit_poc;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/services")
public class ServiceLayerServices {
	@POST
	@Path("/serviceOne/{parm}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ServiceOneResponse serviceOne(@PathParam("parm") String parm, ServiceOneRequest request) {
		System.out.println(parm);
		System.out.println(request.getProperty1());
		System.out.println(request.getProperty2());
		ServiceOneResponse sr = new ServiceOneResponse();
		sr.setProperty1(request.getProperty1().concat(parm));
		sr.setProperty2(request.getProperty2());
		return sr;
	}
}
